﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float speedx;
    private float speed;
	
	void Start ()
    {
        Cursor.lockState = CursorLockMode.Locked;
        GetComponent<Renderer>().enabled = false;
    }
	
	
	void Update ()
    {
        float moveVertical = Input.GetAxis("Vertical") * speed;
        float moveHorizontal = Input.GetAxis("Horizontal") * speed;
        moveVertical *= Time.deltaTime;
        moveHorizontal *= Time.deltaTime;

        transform.Translate(moveHorizontal, 0, moveVertical);





        if (Input.GetKeyDown("escape"))
            Cursor.lockState = CursorLockMode.None;

        if (Input.GetKey("x"))
            speed = speedx * 2;

        else
            speed = speedx;


	
                
    }   
}
